# Configurations for Text editors/IDEs should go in their own directory in the 'dotfiles' directory
## Do not include
+ binaries
+ plugin files
+ Shell Configs
+ Window Manager Configs
+ Debugger Configs

## Other guidelines
+ keep your configs simple
+ keep them relatively short
+ if there are plugins have your config auto install them for the users
